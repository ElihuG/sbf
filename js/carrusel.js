const h = window.hyperapp.h
const app = window.hyperapp.app

const titulos = [
    "preparacion", "beneficios"];
const urls = [ 
    "img/preparacion.png",
    "img/beneficio_antienvejecimiento.png",
    "img/beneficio_antioxidante.png",
    "img/beneficio_comidaalcalina.png",
    "img/beneficio_control.png",
    "img/beneficio_desintoxicante.png",
    "img/beneficio_digestion.png",
    "img/beneficio_energia.png",
    "img/beneficio_piel.png",
];

const state = {
    count: 0,
    nombre: "Preparación",
    imagen: urls[0]
};

const actions = {
    transiciones: value => state => {
        const c = (state.count + value) % urls.length;
        var nom;
        
        if (c != 0){ 
            nom = "Beneficios"; 
        } else { 
            nom = "Preparación"; 
        }

        return { nombre: nom, imagen: urls[c], count: c };
    },
};

const view = (state, actions) => 
    h("div", { class: "col-lg-6 col-md-6 col-sm-5 col-xs-5 preparacion " }, [
        h("h3", {style:{ color: "rgb(227, 168, 43)" }}, state.nombre),
        h("img", {
            class: "img-responsive center-block imagenPreparacion custom-carr",
            align: "center",
            src: state.imagen
        })
    ])

const carr = app(state, actions, view, document.getElementById('comp1'));

setInterval(carr.transiciones, 4000, 1);